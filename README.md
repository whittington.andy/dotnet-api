# Ubuntu 18.04, aspnetcore 3.1 webapi, emacs, firefox, docker, azure data studio and the command line.

### An attempt to create the api tutorial at [Microsoft's](/https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-web-api?view=aspnetcore-3.1&tabs=visual-studio-code) site.

Project creation:

```shell
andy@andyLinuxbox:~/dotnet$ git clone git@gitlab.com:whittington.andy/dotnet-api.git
Cloning into 'dotnet-api'...
warning: You appear to have cloned an empty repository.
andy@andyLinuxbox:~/dotnet$ cd dotnet-api
andy@andyLinuxbox:~/dotnet/dotnet-api$ touch README.md
andy@andyLinuxbox:~/dotnet/dotnet-api$ dotnet new webapi -o .
```

Added required packages:

```shell
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package Microsoft.EntityFrameworkCore.InMemory
```

Build and run project:

```shell
andy@andyLinuxbox:~/dotnet/dotnet-api$ dotnet build
Microsoft (R) Build Engine version 16.6.0+5ff7b0c9e for .NET Core
Copyright (C) Microsoft Corporation. All rights reserved.

  Determining projects to restore...
  Restored /home/andy/dotnet/dotnet-api/dotnet-api.csproj (in 360 ms).
  dotnet-api -> /home/andy/dotnet/dotnet-api/bin/Debug/netcoreapp3.1/dotnet-api.dll

Build succeeded.
    0 Warning(s)
    0 Error(s)

Time Elapsed 00:00:02.43
andy@andyLinuxbox:~/dotnet/dotnet-api$ dotnet run
info: Microsoft.Hosting.Lifetime[0]
      Now listening on: https://localhost:5001
info: Microsoft.Hosting.Lifetime[0]
      Now listening on: http://localhost:5000
info: Microsoft.Hosting.Lifetime[0]
      Application started. Press Ctrl+C to shut down.
info: Microsoft.Hosting.Lifetime[0]
      Hosting environment: Development
info: Microsoft.Hosting.Lifetime[0]
      Content root path: /home/andy/dotnet/dotnet-api
```

Working with [restclient](https://github.com/pashky/restclient.el) major mode in emacs.

The GET request in the bottom pane of emacs. The return on the top.

Service running in the terminal.

![WorkingService](Images/WorkingService.png)

---

### [Add a model](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-web-api?view=aspnetcore-3.1&tabs=visual-studio#add-a-model-class).

At this point I went with a [docker mssql server](https://hub.docker.com/_/microsoft-mssql-server).

The [Docker installation](https://docs.docker.com/engine/install/ubuntu/) is straight forward.

```script
andy@andyLinuxbox:~/dotnet/dotnet-api$ docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=**********' -p 1433:1433 --name mssql2017 -d mcr.microsoft.com/mssql/server:2017-latest

05e16118aeb3bbbfbc087b03122a3ff47c7c81ad675c981e378061a8091c4f78

andy@andyLinuxbox:~/dotnet/dotnet-api$ docker ps
CONTAINER ID        IMAGE                                        COMMAND                  CREATED             STATUS              PORTS                    NAMES
05e16118aeb3        mcr.microsoft.com/mssql/server:2017-latest   "/opt/mssql/bin/nonr…"   8 seconds ago       Up 4 seconds        0.0.0.0:1433->1433/tcp   mssql2017
```
If your container just dies after a few seconds, it's because your password doesn't meet the password requirements.

You can check with docker ps, to make sure that the container is up.

---
### SQL -- Azure Data Studio

I'm going to connect to the newly running docker container from the outside with [Azure Data Studio](https://docs.microsoft.com/en-us/sql/azure-data-studio/download-azure-data-studio?view=sql-server-ver15#get-azure-data-studio-for-linux).

Here is the login:

![AzureDataStudio](Images/AzureDataStudio.png)

After logging in, you should have a clean ballpark. 

![CleanBallPark](Images/CleanBallPark.png)

Lets create a database:

```sql
create DATABASE TodoDB;
```
![CreateDatabase](Images/CreateDatabase.png)

The new database:

![NewDB](Images/NewDB.png)

We need a login for the sql connection from the api.

```sql
CREATE LOGIN Andy WITH PASSWORD = 'P@ssw0rd'
GO
```
![CreateLogin](Images/CreateLogin.png)

The [create user/ addrole member](https://stackoverflow.com/questions/1601186/sql-server-script-to-create-a-new-user) below make us owner of the TodoDB;

```sql
Use TodoDB;
GO

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Andy')
BEGIN
    CREATE USER [Andy] FOR LOGIN [Andy]
    EXEC sp_addrolemember N'db_owner', N'Andy'
END;
GO
```
![SQLLogin](Images/SQLLogin.png)

Should be able to manipulate the database with that user login at this point.

I played with it for a little while, creating a table and then deleting it.

Then deleting the container entirely and rebuilding it from scratch.

---

### Onward.

Modify the Startup.cs.

```c#
	public void ConfigureServices(IServiceCollection services)
	{
		services.AddDbContext<TodoContext>(opt =>
			opt.UseSqlServer(Configuration.GetConnectionString("TodoContext")));
		services.AddControllers();
	}
```
Add a connection string to the application.json.

```json
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
    "AllowedHosts": "*",
    "ConnectionStrings": {
	"TodoContext": "Server=localhost;Database=TodoDB;User Id=Andy;Password=P@ssw0rd;Trusted_Connection=false;MultipleActiveResultSets=true"
    }
}
```
Trusted Connection to the container needs to be false. The api service throws a kerberos error when connecting if true.

---

If you run into trouble with the scaffolding tool [check here](https://github.com/dotnet/Scaffolding/issues/1384).

I had to downgrade the dotnet-aspnet-codegenerator.

```script
dotnet tool uninstall --global dotnet-aspnet-codegenerator
dotnet tool install --global dotnet-aspnet-codegenerator --version 3.1.0
```
---

Build the project.

```script
dotnet build
```
Now we need to do an ef migration and db update.

```script
dotnet ef migrations add InitialCreate
dotnet ef database update
```
Should return something like:

![EFMigrationUpdate](Images/EFMigrationUpdate.png)


```script
dotnet build
dotnet run
```
This screenshot is a profile of the sql connection to the docker container running from the asp.net core webapi. I am hitting the controller from emacs and the return is the 201 created return.

![DockerDBWorking](Images/DockerDBWorking.png)

---

### [Prevent Over-posting](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-web-api?view=aspnetcore-3.1&tabs=visual-studio#prevent-over-posting)

The code portion of the section is mostly replacing TodoItem with TodoItemDTO.

Some return values change a tiny bit. 

The thing that I struggled with was the database.

In the webapi documention there is a no mention of having to migrate anything.

You have to migrate the db to the docker container. 

The documents are a little light for the api stuff, its around other places.

```script
dotnet ef migrations add DTO
dotnet ef database update
```

---

### [Call an ASP.NET Core web API with JavaScript](https://docs.microsoft.com/en-us/aspnet/core/tutorials/web-api-javascript?view=aspnetcore-3.1) -- up next.

The next section is just two line adds in Startup.cs, two directory adds wwwroot and js inside that. Then two file adds index.html in wwwroot and site.js inside wwwroot/js/.

The launchsettings.json looks like this:

```json
    "dotnet_api": {
      "commandName": "Project",
      "launchBrowser": true,
      "applicationUrl": "https://localhost:5001;http://localhost:5000",
      "environmentVariables": {
        "ASPNETCORE_ENVIRONMENT": "Development"
      }
```
Inside the wwwroot/js/site.js there is a connection string here:

```js
const uri = 'https://localhost:5001/api/TodoItems';
```

I had to use the secure connection or suffer cross-site connection failures.

Here is the service running with the js front-end making a call and the sql profiler picking up the  changes.

![CallAPIUsingJS](Images/JS.Call.API.png)


---

### [Adding swashbuckle](https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-3.1&tabs=netcore-cli)

Adding the package is the main deal here. 

```script
dotnet add dotnet-api.csproj package Swashbuckle.AspNetCore -v 5.5.0
```

Add hooks to the service and then the app.

Just beyond that is a bit of a "Tip" to set the variable RoutePrefix to string.Empty.

Don't do that.

```c#
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
    // NO ----> c.RoutePrefix = string.Empty;
});
```

Here is the api returning a get request from swagger.

![SwaggerUI](Images/SwaggerUI.png)

The code is in a working state.

---

Thanks for your time!
